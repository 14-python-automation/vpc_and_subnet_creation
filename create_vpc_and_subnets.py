import boto3

ec2_client = boto3.client('ec2', region_name="ca-central-1")
ec2_resource = boto3.resource('ec2', region_name="ca-central-1")


new_vpc = ec2_resource.create_vpc(
    CidrBlock = "10.0.10.0/16"
)
new_vpc.create_subnet(
    CidrBlock = "10.0.20.0/24"
)
new_vpc.create_subnet(
    CidrBlock = "10.0.30.0/24"
)
new_vpc.create_subnet(
    CidrBlock = "10.0.40.0/24"
)

new_vpc.create_tags(
    Tags=[
        {
            'Key': 'Name',
            'Value': 'dev_vpc'
        },
    ]
)



available_vpcs = ec2_client.describe_vpcs()["Vpcs"]

for vpc in available_vpcs:
    print(vpc["VpcId"])
    cidr_block_assoc_sets = vpc['CidrBlockAssociationSet']
    for assoc_set in cidr_block_assoc_sets:
        print(assoc_set['CidrBlockState'])





